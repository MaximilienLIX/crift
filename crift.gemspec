files_path = {
    :lib_path => "lib/",
    :exception => "lib/exception/",
    :model_path => "lib/model/",
    :view_path => "lib/view/",
    :templates_path => "lib/view/templates/",
    :controller_path => "lib/controller/",
    :assets_path => "lib/view/templates/assets/",
    :css_path => "lib/view/templates/assets/css/",
    :img_path => "lib/view/templates/assets/img/",
    :js_path => "lib/view/templates/assets/js/"
}

crift_files = Array.new(0)

files_path.each do |key, value|
  Dir.foreach(value) do |files|
    crift_files.push(value + files) #if value != "lib/view/templates"
  end
end

Gem::Specification.new do |spec|
  spec.name = 'crift'
  spec.version = '0.1'
  spec.date = '2017-06-16'
  spec.summary = "Code Rift"
  spec.description = "Parser which analyze vulnerabilities with C standard functions."
  spec.authors = ["Maximilien DI DIO", "Roxanne THOMAS", "Guillaume JOVELIN"]
  spec.email = 'maximiliendidio@protonmail.com'
  spec.files = crift_files
  spec.executables << 'crift'
  spec.license = 'MIT'
  spec.add_dependency('colorize')
  spec.add_dependency('sqlite3')
  spec.add_dependency('catpix')
end
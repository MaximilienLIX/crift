require "crift"

RSpec.describe ParserController do
  before(:all) do
    @test = ParserController.new("file.c", ["file.c", "file2.c"])
  end

  context "Test authors array" do
    it "The author must be Roxanne THOMAS" do
      expect(@test.authors_c).to eq(["Roxanne THOMAS "])
    end
  end
end
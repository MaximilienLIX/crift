require "crift"

RSpec.describe ArgumentsController do
  before(:all) do
    @test_simple = ArgumentsController.new(["file.c", "-pre"])
    @test_other = ArgumentsController.new(["file.c", "file2.c", "-dat", "-aut"])
  end

  context "With simple C file" do
    it "files array attribute contains C file" do
      expect(@test_simple.files).to eq(["file.c"])
    end
  end

  context "With a simple -pre argument" do
    it "pre attribute must be true" do
      expect(@test_simple.preprocessor).to eq(true)
    end
  end

  context "With 2 C files" do
    it "files array attribute contains C files" do
      expect(@test_other.files).to eq(["file.c", "file2.c"])
    end
  end

  context "With 2 arguments" do
    it "-dat and -aut must be true" do
      expect(@test_other.dates).to eq(true)
      expect(@test_other.authors).to eq(true)
    end
  end
end
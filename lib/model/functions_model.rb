class FunctionsModel < Model
  private

  @name # type string, primary key
  @description # type string
  @prototype
  @library

  public

  attr_reader(:name, :description, :prototype, :library)

  def initialize(libraries)
    super(File.dirname(__FILE__) + "/functions.db")
    @name = Array.new(0)
    @description = Array.new(0)
    @prototype = Array.new(0)
    @library = Array.new(0)
    libraries.each do |library|
      statement = @database_connection.prepare("SELECT * FROM Functions WHERE Functions.library LIKE ?;")
      statement.bind_param 1, library
      return_statement = statement.execute
      return_statement.each do |hash|
        @data.push(hash)
        @name.push(hash["name"])
        @description.push(hash["description"])
        @prototype.push(hash["prototype"])
        @library.push(hash["library"])
      end
    end
  end
end
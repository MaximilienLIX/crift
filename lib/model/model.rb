# This is the model main class
# It use to respect the MVC pattern

class Model
  protected  # Les classes filles doivent héritées du nom de la base et de la connexion

  @data # Tableau de hashs

  @database_name
  @database_connection

  public

  attr_reader(:data, :database_name, :database_connection)

  def initialize(database_name)
    begin
      @data = Array.new(0)
      self.test_database(database_name)
      @database_connection = SQLite3::Database.new(@database_name)
      @database_connection.results_as_hash = true # Pour récupérer les données sous la forme d'un hash
    rescue DatabaseNotFoundException => e  # Si on ne trouve pas la base de données par défault
      abort(e.stacktrace)
    end
  end

  def test_database(database_name)
    raise DatabaseNotFoundException.new(database_name) if !File.exist?(database_name.to_s)
    @database_name = database_name
  end

  def get_by_index(index, columns)
    res = Hash.new(0)
    columns.each do |column|
      res[column] = @data[index][column.to_s]
    end
    return res
  end

  def select(*columns)
    res =  Array.new(0)
    @data.each do |hash|
      arr = Array.new(0)
      columns.each do |column|
        arr.push(hash[column])
      end
      res.push(arr)
    end
    return res
  end

  def search(exp, colin, *colout)
    res = Hash.new(0)
    @data.each do |hash|
      if exp.include?(hash[colin])
        res = { colin => hash[colin] }
        colout.each do |out|
        #res = { colin => hash[colin], colout => hash[colout] }
          res[out] = hash[out]
        end
      end
    end
    return res
  end
end
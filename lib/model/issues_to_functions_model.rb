class IssuesToFunctions < Model
  private

  @function_name
  @id
  @description

  public

  attr_reader(:function_name, :id, :description)

  def initialize(functions)
    super(File.dirname(__FILE__) + "/functions.db")
    @function_name = Array.new(0)
    @id = Array.new(0)
    @description = Array.new(0)
    @in_file = Array.new(0)
    functions.each do |function|
      statement = @database_connection.prepare("SELECT IssuesToFunctions.name_function , Issues.id, Issues.description FROM IssuesToFunctions
                                                JOIN Issues ON IssuesToFunctions.id_issue = Issues.id
                                                WHERE IssuesToFunctions.name_function LIKE ?;")
      statement.bind_param 1, function["function"]["name"]
      return_statement = statement.execute
      return_statement.each do |hash|
        @data.push("database" => hash, "file" => function)
        @function_name.push(hash["name_function"])
        @id.push(hash["id"])
        @description.push(hash["description"])
      end
    end
  end
end
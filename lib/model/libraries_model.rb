class LibrariesModel < Model
  private

  @name # type string, primary key
  @description # type string

  public

  attr_reader(:name, :description)

  def initialize(libraries)
    super(File.dirname(__FILE__) + "/functions.db")
    @name = Array.new(0)
    @description = Array.new(0)
    @library = Array.new(0)
    libraries.each do |library|
      statement = @database_connection.prepare("SELECT * FROM Libraries WHERE Libraries.name LIKE ?;")
      statement.bind_param 1, library
      return_statement = statement.execute
      return_statement.each do |hash|
        @data.push(hash) # Initialisation de @data. On récupère les données
        @name.push(hash["name"])
        @description.push(hash["description"])
      end
    end
  end
end
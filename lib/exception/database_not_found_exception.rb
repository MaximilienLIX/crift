class DatabaseNotFoundException < RuntimeError
  private

  @stacktrace

  public

  attr_reader(:stacktrace)

  def initialize(db_name)
    super
    @stacktrace = "#{db_name.to_s} doesn't exist!"
  end
end

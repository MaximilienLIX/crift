class FileNotFoundException < RuntimeError
  private

  @stacktrace

  public

  attr_reader(:stacktrace)

  def initialize(file_name)
    super
    @stacktrace = "#{file_name.to_s} doesn't exist!"
  end
end
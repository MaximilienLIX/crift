class NotEnoughArgumentsException < RuntimeError

  private

  @stacktrace

  public

  attr_reader(:stacktrace)

  def initialize(args_controller)
    super
    @stacktrace = "Number of arguments #{args_controller.size}. It's not enough!"
  end
end
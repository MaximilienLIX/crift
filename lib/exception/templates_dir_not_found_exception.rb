class TemplatesDirNotFoundException < RuntimeError
  private

  @stacktrace

  public

  attr_reader(:stacktrace)

  def initialize(templates_dir)
    super
    @stacktrace = "#{templates_dir} directory not found!"
  end
end
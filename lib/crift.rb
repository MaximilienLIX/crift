require 'sqlite3'
require 'erb'
require 'catpix'
require 'colorize'
require 'fileutils'

require_relative 'controller/controller'
require_relative 'model/model'
require_relative 'view/view'

require_relative 'controller/parser_controller'
require_relative 'controller/arguments_controller'
require_relative 'model/libraries_model'
require_relative 'model/functions_model'
require_relative 'model/issues_to_functions_model'
require_relative 'view/console_stream'
require_relative 'view/html_stream'

require_relative 'exception/file_not_found_exception'
require_relative 'exception/database_not_found_exception'
require_relative 'exception/not_enough_arguments_exception'
require_relative 'exception/templates_dir_not_found_exception'

# Main class

class Crift

  private
  @version
  @platform

  @args_controller    # controller for arguments

  public

  attr_reader(:args_controller) # accessor readonly

  def initialize(args)
    @version = "0.1alpha"
    @platform = RUBY_PLATFORM

    puts "
 ██████╗ ██████╗ ██████╗ ███████╗    ██████╗ ██╗███████╗████████╗
██╔════╝██╔═══██╗██╔══██╗██╔════╝    ██╔══██╗██║██╔════╝╚══██╔══╝
██║     ██║   ██║██║  ██║█████╗      ██████╔╝██║█████╗     ██║
██║     ██║   ██║██║  ██║██╔══╝      ██╔══██╗██║██╔══╝     ██║
╚██████╗╚██████╔╝██████╔╝███████╗    ██║  ██║██║██║        ██║
 ╚═════╝ ╚═════╝ ╚═════╝ ╚══════╝    ╚═╝  ╚═╝╚═╝╚═╝        ╚═╝ ".colorize(:red) + "\nversion #{@version} on #{@platform}\n".colorize(:light_red)
    @parsestack = Array.new(0)
    @args_controller = ArgumentsController.new(args)

    console = Array.new(0)

    thr = (0...@args_controller.files.size).map do |i|
      Thread.new(i) do |i|
        parser = ParserController.new(args_controller.files[i],
                                      args_controller.files,
                                      proto: args_controller.proto,
                                      doc: args_controller.doc,
                                      authors: args_controller.authors,
                                      returns: args_controller.returns,
                                      dates: args_controller.dates,
                                      preprocessor: args_controller.preprocessor)
        console.push(:order => i, :message => parser.out)
      end
    end
    thr.each { |t| t.join }

    console.sort_by! { |hash| hash[:order] }

    console.each do |hash|
      puts "#{hash[:message]}"
    end
  end
end

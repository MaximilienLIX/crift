# This class is used to parse a source code in language C

class ParserController < Controller

  private

  @source_code # program source code
  @words_number # number of words
  @lines_number # number of lines
  @source_code_filtered # program source code without comment
  @words_list # the world list of the program source code
  @authors
  @dates
  @returns
  @preprocessor
  @lines_commentary
  @out

  public

  attr_reader(:source_code, :words_number, :lines_number,
              :source_code_filtered, :libc_used_from_file,
              :libc_database, :words_list, :file_name,
              :authors_c, :dates_c, :returns_c, :preprocessor_c,
              :lines_commentary, :out)

  def get_special_comment(line, keyword)
    state = false
    if line[:line].include?(keyword)
      str = ""
      arr_str = Array.new(0)
      line_filtered = ""

      if keyword == "#include"
        line_filtered = line[:line].tr('<>', ' ')
      else
        line_filtered = line[:line]
      end

      words = line_filtered.split
      words.size.times do |i|
        arr_str.push(words[i]) if state
        if words[i] == keyword
          state = true
        end
      end
      state = false
      str = arr_str[0]
      (1...arr_str.size).map do |i|
        str = str + " " + arr_str[i]
      end
      @authors_c.push({:content => str, :nb_line => line[:nb_line]}) if keyword == "@author"
      @returns_c.push({:content => str, :nb_line => line[:nb_line]}) if keyword == "@return"
      @dates_c.push({:content => str, :nb_line => line[:nb_line]}) if keyword == "@date"
      @preprocessor_c.push(str) if keyword == "#include"
    end
  end

  def commentary_statement
    state = true
    nb_line = 0

    @source_code.each_line do |line|
      nb_line += 1
      if line.include?("/*")
        state = false
      end
      @lines_commentary.push(nb_line) if !state
      if state
        if !line.include?("//")
          @source_code_filtered.push(line)
        else
          @lines_commentary.push(nb_line)
        end
      end
      if line.include?("*/")
        state = true
      end
      self.get_special_comment({:line => line, :nb_line => nb_line}, "@author")
      self.get_special_comment({:line => line, :nb_line => nb_line}, "@return")
      self.get_special_comment({:line => line, :nb_line => nb_line}, "@date")
      self.get_special_comment({:line => line, :nb_line => nb_line}, "#include")
    end
  end

  def analyze_each_word
    @source_code.each_line do |line|
      @lines_number += 1
      words = line.split
      words_to_line = 0
      words.each do |word|
        @words_number += 1
        words_to_line += 1
        @words_list.push({"line" => @lines_number, "column" => words_to_line, "number" => @words_number, "word" => word})
      end
    end
  end

  def read_the_code
    @source_code = File.read(@file_name)
    self.commentary_statement
    self.analyze_each_word
  end

  def catch_functions(functions_database)
    functions_file = Array.new(0)
    functions_database.each do |function_database|
      @words_list.each do |hash|
        arr = hash["word"].tr('(', ' ').split
        if function_database["name"].casecmp?(arr[0]) && !@lines_commentary.include?(hash["line"])
          functions_file.push({"function" => function_database,
                               "position" => {"line" => hash["line"],
                                              "column" => hash["column"]}})
        end
      end
    end
    return functions_file
  end
  
  def initialize(file_name, files, proto: false,
                 doc: false, authors: false, returns: false,
                 dates: false, preprocessor: false)
    begin
      self.test_file(file_name)
      @words_list = Array.new(0)
      @words_number = 0
      @lines_number = 0
      @lines_commentary = Array.new(0)
      @source_code_filtered = Array.new(0)

      @authors_c = Array.new(0)
      @returns_c = Array.new(0)
      @dates_c = Array.new(0)
      @preprocessor_c = Array.new(0)

      self.read_the_code

      libraries_model = LibrariesModel.new(@preprocessor_c)
      functions_model = FunctionsModel.new(libraries_model.name)

      functions_from_file = self.catch_functions(functions_model.data)
      issues_model = IssuesToFunctions.new(functions_from_file)

      if doc
        html_stream = HtmlStream.new(issues_model.data, file_name, files, @authors_c,
                                     @returns_c, @dates_c, @source_code,
                                     @source_code_filtered, {:aut => @authors_c,
                                                             :dat => @dates_c,
                                                             :ret => @returns_c,
                                                             :pre => @preprocessor_c})
        @out = html_stream.out_stream
      else
        console = ConsoleStream.new(issues_model.data, {:aut => @authors_c,
                                                        :dat => @dates_c,
                                                        :ret => @returns_c,
                                                        :pre => @preprocessor_c},
                                    file_name, proto: proto, authors: authors,
                                    returns: returns, dates: dates, preprocessor: preprocessor)
        @out = console.out_stream
      end

    rescue DatabaseNotFoundException, FileNotFoundException => e
      ConsoleStream.error(e.stacktrace)
    end

  end
end
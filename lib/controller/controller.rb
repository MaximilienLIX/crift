# This is the controller main class
# It use to respect the MVC pattern

class Controller
  protected

  @file_name # contains the file name

  public

  attr_reader(:db_name, :file_name)

  def test_file(file_name)
    raise FileNotFoundException.new(file_name) if !File.exist?(file_name)
    @file_name = file_name
  end

  def test_args(args)
    raise NotEnoughArgumentsException.new(args) if args.size < 1
  end

end
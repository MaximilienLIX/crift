class ArgumentsController < Controller
  private

  @args
  @files
  @proto
  @doc
  @authors
  @returns
  @dates
  @preprocessor
  @help

  public

  attr_reader(:args, :files, :proto,
              :doc, :authors, :returns,
              :dates, :preprocessor, :help)

  def initialize(args)
    begin
      self.test_args(args)
      @args = args
      @files = Array.new(0)
      @proto = false
      @doc = false
      @authors = false
      @help = false
      @args.each do |arg|
        if arg == "--prototype" || arg == "-pro"
          @proto = true
        elsif arg == "--document" || arg == "-doc"
          @doc = true
        elsif arg == "--authors" || arg == "-aut"
          @authors = true
        elsif arg == "--returns" || arg == "-ret"
          @returns = true
        elsif arg == "--dates" || arg == "-dat"
          @dates = true
        elsif arg == "--preprocessor" || arg == "-pre"
          @preprocessor = true
        else
          @files.push(arg)
        end
      end
    rescue NotEnoughArgumentsException => e
      ConsoleStream.error(e.stacktrace)
    end
  end
end
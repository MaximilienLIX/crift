# This is the view main class
# It use to respect the MVC pattern

class View
  protected

  @out_stream

  public

  attr_reader(:out_stream)

  def initialize(content)
    @out_stream = content
  end
end
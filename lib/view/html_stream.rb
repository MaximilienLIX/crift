# This view is used to print data into static html files.

class HtmlStream < View
  private

  @templates
  @renderer
  @css_bt_included
  @js_bt_included
  @js_jquery_included
  @js_prism_included
  @css_custom_included
  @css_prism_included
  @img_ruby
  @website_title
  @source_code
  @source_code_filtered
  @authors
  @vulnerabilities

  public

  attr_reader(:templates, :renderer, :css_included,
              :js_bt_included, :js_jquery_included,
              :js_prism_included, :css_custom_included,
              :css_prism_included, :img_ruby, :website_title,
              :authors, :vulnerabilities)

  def list_file_menu(files)
    list_files = Array.new(0)
    files.each do |file|
      list_files.push(file.gsub(/.c/, '.html'))
    end
    return list_files
  end

  def get_info(array_hash, str)
    aut = ""
    aut_t = str
    i = 0
    array_hash.each do |hash|
      aut += "#{hash[:content]} "
      i += 1
    end
    (i > 1) ? aut_t += "s: " + aut : aut_t += ": " + aut
    return aut_t
  end

  def initialize(issues, file_name, files,
                 authors, returns, dates,
                 source_code, source_filtered, infos)
    @css_bt_included = "assets/css/bootstrap.min.css"
    @css_custom_included = "assets/css/custom.css"
    @css_prism_included = "assets/css/prism.css"
    @js_bt_included = "assets/js/bootstrap.min.js"
    @js_jquery_included = "assets/js/jquery-3.2.js"
    @js_prism_included = "assets/js/prism.js"
    @img_ruby = "assets/img/logoRed.png"
    @website_title = "Code Rift Report"
    @source_code = source_code.to_s.gsub(/</, '&#60;')
    @source_code_filtered = source_filtered.map { |s| "#{s}"}.join('').gsub(/</, '&#60;')
    @authors = self.get_info(infos[:aut], "Author")
    @comments = self.get_info(infos[:dat], "Date")

    @vulnerabilities = issues

    doc_folder = Dir.mkdir("doc") if !File.directory?("doc")
    template_html = File.dirname(__FILE__) + "/templates/index.html.erb"
    @renderer = ERB.new(File.read(template_html))

    script_path = File.dirname(__FILE__)
    current_path = Dir.pwd

    FileUtils.cp_r(script_path + '/templates/assets', current_path + '/doc')

    file_html = file_name.tr(".c", "")
    list_files = self.list_file_menu(files)

    doc_html = File.new(current_path + "/doc/#{file_html}.html", "w+")
    doc_html.write(@renderer.result(binding))

    doc_html.close

    super("Documentation generated successfully in".colorize(:yellow) + " #{current_path}/doc/#{file_html}.html".colorize(:light_blue))
  end
end
# This view is used to print data into the console

class ConsoleStream < View
  public

  def help
    return "
COMMAND LIST\n
crift [options]
There is no order for options\n
OPTIONS AVAILABLE\n
--prototype or -pro
To have function prototype\n
--document or -doc
To generate html documentation\n
--authors or --aut
To get authors\n
--returns or -ret
To get the return messages\n
--dates or -dat
To get the data\n
--preprocessor or -pre
To get the libraries declared\n
--help or -h
To get help\n"
  end

  def initialize(issues, info_hash, file,
                 proto: false, authors: false, returns: false,
                 dates: false, preprocessor: false)

    message = "File -> " + "#{file}\n".colorize(:light_green)

    if authors
      aut = ""
      aut_t = "| author"
      i = 0
      info_hash[:aut].each do |hash|
        aut += "#{hash[:content]}".colorize(:light_blue) + " line".colorize(:blue) + " #{hash[:nb_line]} ".colorize(:light_blue)
        i += 1
        aut_t + "s" if i > 1
      end
      message += aut_t.colorize(:blue) + ":".colorize(:blue) + " #{aut}\n"
    end

    if returns
      aut = ""
      aut_t = "| return"
      i = 0
      info_hash[:ret].each do |hash|
        aut += "#{hash[:content]}".colorize(:light_blue) + " line".colorize(:blue) + " #{hash[:nb_line]} ".colorize(:light_blue)
        i += 1
      end
      aut_t + "s" if i > 1

      message += aut_t.colorize(:blue) + ":".colorize(:blue) + " #{aut}\n"
    end

    if dates
      aut = ""
      aut_t = "| date"
      i = 0
      info_hash[:dat].each do |hash|
        aut += "#{hash[:content]}".colorize(:light_blue) + " line".colorize(:blue) + " #{hash[:nb_line]} ".colorize(:light_blue)
        i += 1
      end
      aut_t + "s" if i > 1
      message += aut_t.colorize(:blue) + ":".colorize(:blue) + " #{aut}\n"
    end

    if preprocessor
      aut = ""
      aut_t = "| librar"
      i = 0
      info_hash[:pre].each do |hash|
        aut += "#{hash} ".colorize(:light_blue)
        i += 1
      end
      (i > 1) ? aut_t += "ies" : aut_t += "y"
      message += aut_t.colorize(:blue) + ":".colorize(:blue) + " #{aut}\n"
    end

    if authors || returns || dates || preprocessor
      message += "\n"
    end

    issues.each do |hash|
      message += "| function:".colorize(:yellow) + " #{hash["database"]["name_function"]}\n".colorize(:light_blue)
      message += "| prototype:".colorize(:yellow) + " #{hash["file"]["function"]["prototype"]}\n".colorize(:light_blue) if proto
      message += "| line:".colorize(:yellow) + " #{hash["file"]["position"]["line"]}".colorize(:light_blue)
      message += " nb:".colorize(:yellow) + " #{hash["file"]["position"]["column"]}\n".colorize(:light_blue)
      message += "| warning:".colorize(:light_red) + " #{hash["database"]["description"]}\n\n".colorize(:light_blue)
    end
    super(message)
  end

  def self.error(stacktrace)
    abort(stacktrace)
  end
end
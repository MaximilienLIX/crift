![crift logo](/lib/view/templates/assets/img/logo.png)
# Code Rift - EN
> A MVC ruby parser for C programming files

## What's the goal ?
Code Rift have to check the functions used to warns user about security issues avalaibles. The parser can be used in a shell or in a Ruby script and declared like a library.

## What's the features ?
Code Rift can analyze many files in the same time (multithread) and the console output can be customized
with some options. Special comments can be use in a C file for Code Rift.
``` c
#include <stdio.h>
   /*
    @author Surname Firstname
    @return This is just a stupid hello world
    @date the 23/06/2017
   */
int main(void){
    char buffer[10];
    sprintf(buffer, "HelloWorld");
    printf("%s", buffer);
    return 0;
}
```

The parser can generate a static web documentation ready to be upload on Gitlab pages or Github pages by example.

## The commands

The order of parameters has no importance. There are always 2 ways to write an option.

| full name | simple name | description |
|-----------|-------------|-------------|
|--prototype|-pro         |show function's prototype|
|--document |-doc         |generate the documentation|
|--authors  |-aut         |show the authors|
|--returns  |-ret         |show the return comments|
|--dates    |-dat         |show the dates comments|
|--preprocessor|-pre      |show the libraries used|

```bash
crift hw.c input.c --authors --returns --dates
crift hw.c input.c --document
```

## The context
Code Rift is an university project created by a group of students in computer science.

## How to install ?
A gem is already build to be installed
Just type:
```bash
gem install crift-0.1.gem 
```

But if you want to build the package yourself, just do this
```bash
gem build crift.gemspec
```

# Code Rift - FR
> Un analyseur MVC en Ruby pour des codes sources en C

## Quel est le but ?
Code Rift doit vérifier les fonctions utilisées pour prévenir l'utilisateur des possibles problèmes de sécurité. L'analyseur peut être utilisé dans un shell ou comme un script Ruby en le déclarant comme une bilbliothèque.'

## Quelles sont les fonctionnalités ?
Code Rift peut analyser plusieurs fichiers en même temps (multithread) et la sortie console peut être personnalisée via des options.
Des commentaires spéciaux peuvent être utilisés dans un fichier C pour Code Rift.
``` c
#include <stdio.h>
   /*
    @author Nom Prénom
    @return Ce programme n'est qu'un hello world
    @date le 23/06/2017
   */
int main(void){
    char buffer[10];
    sprintf(buffer, "HelloWorld");
    printf("%s", buffer);
    return 0;
}
```

L'analyseur peut générer une documentation web statique prête à être hébergée sur Gitlab ou Github par exemple.

## Les commandes

L'ordre des paramètres n'a pas d'importance. Il y a toujours 2 façons d'écrire une option.

| nom complet | nom simple | description |
|-----------|-------------|-------------|
|--prototype|-pro         |affiche le prototype de la fonction|
|--document |-doc         |génère une documentation|
|--authors  |-aut         |affiche les auteurs|
|--returns  |-ret         |affiche ce que retourne le programme|
|--dates    |-dat         |affiche la date du programme|
|--preprocessor|-pre      |affiche les bibliothèques utilisées|

```bash
crift hw.c input.c --authors --returns --dates
crift hw.c input.c --document
```

## Le contexte
Code Rift est un projet universitaire créé par un groupe d'étudiant en études sur l'informatique.

## Comment l'installer
Une gem est déjà prête pour être installée:
```bash
gem install crift-0.1.gem 
```

Mais si vous voulez produire la gem vous même alors:
```bash
gem build crift.gemspec
```
